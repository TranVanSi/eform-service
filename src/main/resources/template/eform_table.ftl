<form class="form-horizontal" enctype="multipart/form-data">
    <fieldset>
        <div class="row">
            <legend class="center">${formListName}</legend>
            <table class="table table-bordered" linkthemmoi="${maFormAdd}" linkchitiet="${maFormChiTiet}" id="${maFormAdd}" data-pagination="true" data-toggle="table"
                   data-side-pagination="server" data-mobile-responsive="true" data-ajax="ajaxRequest"
                   data-id-field="_id">
                <thead class="thead-light">
                <tr>
                    <th data-formatter="formatterRowIndex" data-field="stt" class="stt">STT</th>
                    ${thHtml}
                    <th data-formatter="formatterButtons" class="thaoTac">Thao tac</th>
                </tr>
                </thead>
                <tbody id="body${maFormAdd}"></tbody>
            </table>
        </div>
    </fieldset>
</form>