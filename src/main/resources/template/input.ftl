<div class="form-group col-md-4 col-sm-4 col-xs-12">
    <label class="" for="">${labelName}</label>
    <div class="w-100">
        <input id="${inputId}" name="${inputId}" type="text" placeholder="${labelName}"
               class="form-control input-md" lienket="" searchdulieu="" update-status="override">
    </div>
</div>