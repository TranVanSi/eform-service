package vn.sdt.eform.model;

public enum Status {
    FAIL(0),
    SUCCESS(1);
    private final Integer status;

    private Status(int value) {
        this.status = value;
    }

    public int value() {
        return this.status;
    }

    @Override
    public String toString() {
        return status.toString();
    }
}
