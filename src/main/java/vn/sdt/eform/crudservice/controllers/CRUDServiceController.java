package vn.sdt.eform.crudservice.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import vn.sdt.eform.cauhinheform.dto.ResponseDTO;
import vn.sdt.eform.crudservice.model.ServiceData;
import vn.sdt.eform.crudservice.services.CRUDHttpDataService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/servicedata")
public class CRUDServiceController {
    private final CRUDHttpDataService crudHttpDataService;

    @PostMapping
    public ResponseDTO save(@RequestBody ServiceData serviceData) {
        return crudHttpDataService.saveCauHinhCRUDService(serviceData);
    }

    @GetMapping("/findByCauHinhId")
    public ResponseDTO findByCauHinhId(@RequestParam(value = "cauHinhId") String cauHinhId) {
        return crudHttpDataService.findByCauHinhId(cauHinhId);
    }

    @DeleteMapping("/delete")
    public boolean deleteCauHinh(@RequestParam(value = "cauHinhId") String maForm) {
        crudHttpDataService.deleteCauHinh(maForm);
        return true;
    }
}
