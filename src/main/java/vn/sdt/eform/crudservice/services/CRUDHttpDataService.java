package vn.sdt.eform.crudservice.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import vn.sdt.eform.cauhinheform.dto.ResponseDTO;
import vn.sdt.eform.crudservice.dao.CRUDServiceRepository;
import vn.sdt.eform.crudservice.mapper.ServiceDataMapper;
import vn.sdt.eform.crudservice.model.ServiceData;
import vn.sdt.eform.model.Status;

@Service
@RequiredArgsConstructor
public class CRUDHttpDataService {
    private final CRUDServiceRepository crudServiceRepository;
    private final ServiceDataMapper serviceDataMapper;

    public ResponseDTO saveCauHinhCRUDService(ServiceData serviceData) {
        return new ResponseDTO(Status.SUCCESS.value(), crudServiceRepository.save(serviceData));
    }

    public ResponseDTO findByCauHinhId(String cauHinhId) {
        return new ResponseDTO(Status.SUCCESS.value(), serviceDataMapper.serviceDataToServiceDataDTO(crudServiceRepository.findByCauHinhId(cauHinhId)));
    }

    public void deleteCauHinh(String cauHinhId) {
        ServiceData serviceData = crudServiceRepository.findByCauHinhId(cauHinhId);
        if (serviceData != null) {
            crudServiceRepository.delete(serviceData);
        }
    }
}
