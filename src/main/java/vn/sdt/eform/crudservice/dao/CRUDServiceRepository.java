package vn.sdt.eform.crudservice.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import vn.sdt.eform.crudservice.model.ServiceData;

public interface CRUDServiceRepository extends MongoRepository<ServiceData, String> {
    ServiceData findByCauHinhId(String maForm);
}
