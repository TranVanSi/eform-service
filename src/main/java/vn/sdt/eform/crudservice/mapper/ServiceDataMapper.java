package vn.sdt.eform.crudservice.mapper;

import org.mapstruct.Mapper;
import vn.sdt.eform.crudservice.dto.ServiceDataDTO;
import vn.sdt.eform.crudservice.model.ServiceData;

@Mapper(componentModel = "spring")
public interface ServiceDataMapper {
    ServiceDataDTO serviceDataToServiceDataDTO(ServiceData serviceData);
}
