package vn.sdt.eform.crudservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;

@Document(collection = "servicedata")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ServiceData {
    @Id
    private String id;
    private String cauHinhId;
    private String dataJson;
    private String token;
}
