package vn.sdt.eform.crudservice.dto;

import lombok.Data;

@Data
public class ServiceDataDTO {
    private String id;
    private String cauHinhId;
    private String dataJson;
    private String token;
}
