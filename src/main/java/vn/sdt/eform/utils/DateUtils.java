package vn.sdt.eform.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {
    public static Date parseStringToDateFull(String data) throws Exception {
        try {
            SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd");
            return parser.parse(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String parseStringDateToStringMongo(String data) throws Exception {
        try {
            SimpleDateFormat parser = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy");
            SimpleDateFormat parserDay = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            return parserDay.format(parser.parse(data));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  null;
    }

    public static String addDateQuery(String denNgay) throws Exception{
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        c.setTime(sdf.parse(denNgay));
        c.add(Calendar.DAY_OF_MONTH, 1);
        String newDate = sdf.format(c.getTime());
        return newDate;
    }
}
