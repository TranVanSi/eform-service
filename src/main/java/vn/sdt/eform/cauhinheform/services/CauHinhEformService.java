package vn.sdt.eform.cauhinheform.services;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.bson.types.ObjectId;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import vn.sdt.eform.cauhinheform.dao.CauHinhEformRepository;
import vn.sdt.eform.cauhinheform.dto.ResponseDTO;
import vn.sdt.eform.cauhinheform.model.CauHinhEform;
import vn.sdt.eform.cauhinheform.utils.Constants;
import vn.sdt.eform.cauhinheform.utils.EformUtil;
import vn.sdt.eform.cauhinheform.utils.StringUtil;
import vn.sdt.eform.model.Status;
import vn.sdt.eform.taomoiform.dto.DuLieuDTO;
import vn.sdt.eform.taomoiform.services.DuLieuFormService;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CauHinhEformService {
    @Autowired
    DuLieuFormService duLieuFormService;
    private final CauHinhEformRepository cauHinhEformRepository;

    public ResponseDTO findByMa(String ma) {
        return new ResponseDTO(Status.SUCCESS.value(), cauHinhEformRepository.findByMaForm(ma));
    }

    public ResponseDTO saveCauHinhEform(CauHinhEform cauHinhEform) {
        return new ResponseDTO(Status.SUCCESS.value(), cauHinhEformRepository.save(cauHinhEform));
    }

    public ResponseDTO findByKeyWordAndTrangThai(String search, int page, int size) throws Exception {
        Pageable pageable = PageRequest.of(page/size, size);

        return new ResponseDTO(Status.SUCCESS.value(),
                cauHinhEformRepository.findByMaFormContainingAndTrangThaiFormOrTenFormContainingAndTrangThaiForm(
                        URLDecoder.decode(search, "UTF-8"), true, URLDecoder.decode(search, "UTF-8"), true, pageable));
    }

    public ResponseDTO findByKeyword(String search, int page, int size) throws UnsupportedEncodingException {
        Pageable pageable = PageRequest.of(page/size, size);

        return new ResponseDTO(Status.SUCCESS.value(),
                cauHinhEformRepository.findByTenFormContainingOrMaFormContaining(
                        URLDecoder.decode(search, "UTF-8"), URLDecoder.decode(search, "UTF-8"), pageable));
    }

    public void deleteCauHinhEform(String ma) {
        CauHinhEform cauHinhEform = cauHinhEformRepository.findByMaForm(ma);
        if (cauHinhEform != null) {
            cauHinhEformRepository.delete(cauHinhEform);
        }
    }

    public ResponseDTO getDanhSach() {
        return new ResponseDTO(Status.SUCCESS.value(), cauHinhEformRepository.findTop1000By());
    }

    public ResponseDTO findById(String objectId) {
       ObjectId id = new ObjectId(objectId);
        return new ResponseDTO(Status.SUCCESS.value(), cauHinhEformRepository.findById(id));
    }

    public ResponseDTO findAllByLoai(int loai) {
        return new ResponseDTO(Status.SUCCESS.value(), cauHinhEformRepository.findAllByLoai(loai));
    }
}
