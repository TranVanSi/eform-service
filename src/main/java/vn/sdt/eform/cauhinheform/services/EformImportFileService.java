package vn.sdt.eform.cauhinheform.services;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.bson.types.ObjectId;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import vn.sdt.eform.cauhinheform.dao.CauHinhEformRepository;
import vn.sdt.eform.cauhinheform.dto.ResponseDTO;
import vn.sdt.eform.cauhinheform.model.CauHinhEform;
import vn.sdt.eform.cauhinheform.utils.Constants;
import vn.sdt.eform.cauhinheform.utils.EformUtil;
import vn.sdt.eform.cauhinheform.utils.StringUtil;
import vn.sdt.eform.model.Status;
import vn.sdt.eform.taomoiform.dto.DuLieuDTO;
import vn.sdt.eform.taomoiform.services.DuLieuFormService;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
@RequiredArgsConstructor
public class EformImportFileService {
    @Autowired
    DuLieuFormService duLieuFormService;
    @Autowired
    CauHinhEformService cauHinhEformService;

    EformUtil eformUtil = new EformUtil();
    private final CauHinhEformRepository cauHinhEformRepository;

    public ResponseDTO createEform(MultipartFile file) throws IOException, TemplateException {
        Configuration configuration = new Configuration();
        configuration.setClassForTemplateLoading(this.getClass(), "/template");
        Template templateInputHtml = configuration.getTemplate("input.ftl");
        Template templateHtmlAdd = configuration.getTemplate("eform_add.ftl");
        Template templateThHtml = configuration.getTemplate("th.ftl");
        Template templateTable = configuration.getTemplate("eform_table.ftl");
        Template templateInputCollection = configuration.getTemplate("input_collection.flt");
        Template templateCollection = configuration.getTemplate("eform_collection.ftl");
        Template templateTableCollection = configuration.getTemplate("table_collection.flt");
        Template templateLabelCollection = configuration.getTemplate("label_collection.flt");
        Template templateLabelChiTietHtml = configuration.getTemplate("label_chitiet.ftl");

        List<String> listId = new ArrayList<>();
        List<String> listLabel = new ArrayList<>();
        List<DuLieuDTO> duLieuDTOS = new ArrayList<>();

        Workbook workbook = WorkbookFactory.create(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();
        String maForm = "";
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            Iterator<Cell> cellIterator = row.cellIterator();
            DataFormatter dataFormatter = new DataFormatter();
            if (row.getRowNum() == 0) {
                List<String> inputCollection = new ArrayList<>();
                List<String> labelCollection = new ArrayList<>();
                String html = "";
                String labelHtml = "";
                String thTable = "";
                String table = "";
                String tenForm = sheet.getSheetName();
                maForm = StringUtil.removeSpecialKey(StringUtil.removeAccent(StringUtil.upperFirstCharacter(tenForm)));
                String tenFormTable = "Danh sách " + tenForm;
                String tenFormChiTiet = "Chi tiết " + tenForm;
                String maFormTable = "DS_" + maForm;
                String maFormChiTiet = "CT_" + maForm;

                while (cellIterator.hasNext()){
                    Cell cell = cellIterator.next();
                    String label = dataFormatter.formatCellValue(cell);
                    if (label != null && label.length() > 0) {
                        String inputId = StringUtil.lowercaseFirstKey(StringUtil.removeSpecialKey(
                                StringUtil.removeAccent(StringUtil.upperFirstCharacter(label))));
                        listId.add(cell.getColumnIndex(), inputId);
                        listLabel.add(cell.getColumnIndex(), label);
                        inputCollection.add(eformUtil.createInputCollectionSetting(templateInputCollection, label, inputId));
                        labelCollection.add(eformUtil.createLabelCollectionSetting(templateLabelCollection, label, maForm, inputId));
                        html += eformUtil.createInputHtml(templateInputHtml, label, inputId);
                        labelHtml += eformUtil.createLabelHtml(templateLabelChiTietHtml, label, inputId);
                        thTable += eformUtil.createThHtml(templateThHtml, label, inputId);
                    }
                }
                String collectionBody = eformUtil.createCollectionSetting(templateCollection, tenForm,String.join(",", inputCollection));
                String collectionChiTiet = eformUtil.createCollectionSetting(templateCollection, tenFormChiTiet,String.join(",", labelCollection));
                String collectionTable = eformUtil.createCollectionTable(templateTableCollection, tenFormTable, maForm, tenForm, maFormChiTiet,
                        "%5B%22stt%22,%22" + String.join("%22,%22", listId) + "%22,%22thaoTac%22%5D",
                        "%5B%22STT%22,%22" + String.join("%22,%22", listLabel) + "%22,%22Thao tac%22%5D");
                html = eformUtil.createBodyAddForm(templateHtmlAdd, html, tenForm);
                labelHtml = eformUtil.createBodyAddForm(templateHtmlAdd, labelHtml, tenFormChiTiet);
                table = eformUtil.createTable(templateTable, tenFormTable, maForm, thTable);
                CauHinhEform cauHinhEform = new CauHinhEform(null, maForm, tenForm,
                        true, html, String.join(",", listId), collectionBody, Constants.LOAI_FORM.THEM_MOI);

                CauHinhEform cauHinhEformTable = new CauHinhEform(null, maFormTable, tenFormTable,
                        true, table,"", collectionTable, Constants.LOAI_FORM.DANH_SACH);

                CauHinhEform cauHinhEformChiTiet = new CauHinhEform(null, maFormChiTiet, tenFormChiTiet,
                        true, labelHtml,"", collectionChiTiet, Constants.LOAI_FORM.CHI_TIET);
                cauHinhEformService.saveCauHinhEform(cauHinhEform);
                cauHinhEformService.saveCauHinhEform(cauHinhEformTable);
                cauHinhEformService.saveCauHinhEform(cauHinhEformChiTiet);
            } else {
                DuLieuDTO duLieu = new DuLieuDTO();
                JSONObject jsonObject = new JSONObject();
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    int columnIndex = cell.getColumnIndex();
                    String cellValue = dataFormatter.formatCellValue(cell);
                    if (cellValue != null && cellValue.length() > 0) {
                        jsonObject.put(listId.get(columnIndex), cellValue);
                    }
                }
                duLieu.setDuLieu(jsonObject.toString());
                duLieuDTOS.add(duLieu);
            }
        }
        duLieuFormService.saveDuLieuAll(duLieuDTOS, maForm);

        return new ResponseDTO(Status.SUCCESS.value(), maForm);
    }


    public ResponseDTO findById(String objectId) {
        ObjectId id = new ObjectId(objectId);
        return new ResponseDTO(Status.SUCCESS.value(), cauHinhEformRepository.findById(id));
    }
}
