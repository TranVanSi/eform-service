package vn.sdt.eform.cauhinheform.controllers;

import freemarker.template.TemplateException;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import vn.sdt.eform.cauhinheform.dto.ResponseDTO;
import vn.sdt.eform.cauhinheform.model.CauHinhEform;
import vn.sdt.eform.cauhinheform.services.CauHinhEformService;
import vn.sdt.eform.cauhinheform.services.EformImportFileService;

import java.io.IOException;
import java.net.URLDecoder;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/cauhinheform")
public class CauHinhEformController {
    private final CauHinhEformService cauHinhEformService;
    private final EformImportFileService eformImportFileService;

    @PostMapping
    public ResponseDTO save(@RequestBody CauHinhEform cauHinhEform) {
        return cauHinhEformService.saveCauHinhEform(cauHinhEform);
    }

    @PostMapping("/importFile")
    public ResponseDTO importFile(@RequestParam(value = "file") MultipartFile file) throws IOException, TemplateException {
        return eformImportFileService.createEform(file);
    }

    @GetMapping("/findByMa")
    public ResponseDTO findByMa(@RequestParam(value = "ma") String maForm) throws Exception {
        if (maForm != null) {
            maForm = URLDecoder.decode(maForm, "UTF-8");
        }
        return cauHinhEformService.findByMa(maForm);
    }

    @GetMapping("/search")
    public ResponseDTO search(@RequestParam(value = "offset", defaultValue = "0") int offset,
                              @RequestParam(value = "limit", defaultValue = "10") int limit,
                              @RequestParam(value = "search", defaultValue = "") String search,
                              @RequestParam(value = "status", defaultValue = "false") Boolean status) throws Exception {
        if (status) {
            return cauHinhEformService.findByKeyWordAndTrangThai(search, offset, limit);
        }
        return cauHinhEformService.findByKeyword(search, offset, limit);
    }

    @DeleteMapping("/delete")
    public boolean delete(@RequestParam(value = "ma") String maForm) {
        cauHinhEformService.deleteCauHinhEform(maForm);
        return true;
    }

    @GetMapping
    public ResponseDTO getDanhSach() {
        return cauHinhEformService.getDanhSach();
    }

    @GetMapping("/{objectId}")
    public ResponseDTO findById (@PathVariable String objectId) {
        return cauHinhEformService.findById(objectId);
    }

    @GetMapping("/loai/{loai}")
    public ResponseDTO getDanhSachCauHinhByLoai(@PathVariable int loai) throws Exception {
        return cauHinhEformService.findAllByLoai(loai);
    }
}
