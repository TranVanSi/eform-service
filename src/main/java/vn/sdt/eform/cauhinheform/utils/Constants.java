package vn.sdt.eform.cauhinheform.utils;

public class Constants {
    public static interface RESPONE {
        public static final int SUCCESS = 1;
    }
    public static final String DEFAULT_FINAL = "DEFAULT_FINAL";
    public static final String DATA_COLLECTION = "dulieu";

    public static final String SLASH = "/";
    public static final String SPACE = " ";

    public static interface LOAI_FORM {
        public static final int THEM_MOI = 1;
        public static final int DANH_SACH = 2;
        public static final int CHI_TIET = 3;
    }
}
