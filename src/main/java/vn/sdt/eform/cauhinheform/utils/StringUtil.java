package vn.sdt.eform.cauhinheform.utils;

import java.text.Normalizer;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StringUtil {
    public static String removeAccent(String accentString) {
        if (accentString == null) {
            return null;
        }
        String tmp = Normalizer.normalize(accentString, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(tmp).replaceAll("").replaceAll("Đ", "D").replace("đ", "d");
    }
    public static String removeSpecialKey(String str) {
        String resultStr="";
        for (int i=0;i<str.length();i++) {
            if ((str.charAt(i) > 47 && str.charAt(i) < 58) ||
                    (str.charAt(i) > 64 && str.charAt(i) < 91) ||
                    (str.charAt(i) > 96 && str.charAt(i) < 123)) {
                resultStr=resultStr+str.charAt(i);
            }
        }

        return resultStr;
    }

    public static String lowercaseFirstKey(String key) {
        return key.substring(0,1).toLowerCase() + key.substring(1);
    }

    public static String upperFirstCharacter(String words) {
        return Stream.of(words.trim().split("\\s"))
                .filter(word -> word.length() > 0)
                .map(word -> word.substring(0, 1).toUpperCase() + word.substring(1))
                .collect(Collectors.joining(" "));
    }
}
