package vn.sdt.eform.cauhinheform.utils;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class EformUtil {

    public String createTextFromTemplate(Template template, Map<String, Object> model) throws IOException, TemplateException {
        return FreeMarkerTemplateUtils.processTemplateIntoString(template, model);
    }
    public String createInputHtml(Template template, String labelName, String inputId) throws IOException, TemplateException {
        Map<String, Object> model = new HashMap<>();
        model.put("labelName", labelName);
        model.put("inputId", inputId);

        String html = createTextFromTemplate(template, model);

        return html;
    }

    public String createThHtml(Template template, String labelName, String inputId) throws IOException, TemplateException {
        Map<String, Object> model = new HashMap<>();
        model.put("labelName", labelName);
        model.put("inputId", inputId);

        String html = createTextFromTemplate(template, model);

        return html;
    }
    public String createTable(Template template, String formListName, String maFormAdd, String thHtml) throws IOException, TemplateException {
        Map<String, Object> model = new HashMap<>();
        model.put("formListName", formListName);
        model.put("maFormAdd", maFormAdd);
        model.put("maFormChiTiet", "CT_" +maFormAdd);
        model.put("thHtml", thHtml);

        String html = createTextFromTemplate(template, model);

        return html;
    }

    public String createCollectionSetting(Template template, String formName, String collectionBody) throws IOException, TemplateException {
        Map<String, Object> model = new HashMap<>();
        model.put("formName", formName);
        model.put("collectionBody", collectionBody);

        String html = createTextFromTemplate(template, model);

        return html;
    }

    public String createCollectionTable(Template template, String formName, String maFormAdd, String tenFormAdd,
                                        String maFormChiTiet, String listKey, String listLabel) throws IOException, TemplateException {
        Map<String, Object> model = new HashMap<>();
        model.put("formName", formName);
        model.put("maFormAdd", maFormAdd);
        model.put("tenFormAdd", tenFormAdd);
        model.put("maFormChiTiet", maFormChiTiet);
        model.put("listKey", listKey);
        model.put("listLabel", listLabel);

        String html = createTextFromTemplate(template, model);

        return html;
    }

    public String createInputCollectionSetting(Template template, String label, String inputId) throws IOException, TemplateException {
        Map<String, Object> model = new HashMap<>();
        model.put("label", label);
        model.put("inputId", inputId);

        String html = createTextFromTemplate(template, model);

        return html;
    }

    public String createLabelCollectionSetting(Template template, String labelText, String maFormAdd, String truongId) throws IOException, TemplateException {
        Map<String, Object> model = new HashMap<>();
        model.put("labelText", labelText);
        model.put("maFormAdd", maFormAdd);
        model.put("truongId", truongId);

        String html = createTextFromTemplate(template, model);

        return html;
    }

    public String createLabelHtml(Template template, String paramName, String pramId) throws IOException, TemplateException {
        Map<String, Object> model = new HashMap<>();
        model.put("pramId", pramId);
        model.put("paramName", paramName);

        String html = createTextFromTemplate(template, model);

        return html;
    }

    public String createBodyAddForm(Template template, String bodyHtml, String formName) throws IOException, TemplateException {
        Map<String, Object> model = new HashMap<>();
        model.put("formName", formName);
        model.put("bodyHtml", bodyHtml);

        String html = createTextFromTemplate(template, model);

        return html;
    }

    public static boolean isNumeric(String strNum) {
        Pattern pattern = Pattern.compile("-?\\d+(\\.\\d+)?");
        if (strNum == null) {
            return false;
        }
        return pattern.matcher(strNum).matches();
    }
}
