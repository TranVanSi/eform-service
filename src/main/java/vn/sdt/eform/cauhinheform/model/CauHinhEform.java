package vn.sdt.eform.cauhinheform.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;

@Document(collection = "cauhinh")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CauHinhEform {
    @Id
    private String id;
    private String maForm;
    private String tenForm;
    private Boolean trangThaiForm;
    private String html;
    private String listId;
    private String collection;
    private int loai;
}
