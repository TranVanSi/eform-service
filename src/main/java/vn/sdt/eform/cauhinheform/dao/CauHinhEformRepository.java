package vn.sdt.eform.cauhinheform.dao;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import vn.sdt.eform.cauhinheform.model.CauHinhEform;

import java.util.List;

public interface CauHinhEformRepository extends MongoRepository<CauHinhEform, String> {
    CauHinhEform findById(ObjectId id);

    CauHinhEform findByMaForm(String ma);
    List<CauHinhEform> findAllByLoai(int loai);
    Page<CauHinhEform> findByMaFormContainingAndTrangThaiFormOrTenFormContainingAndTrangThaiForm(String tenForm, Boolean trangThaiForm1, String maForm, Boolean trangThaiForm, Pageable pageable);

    Page<CauHinhEform> findByTenFormContainingOrMaFormContaining(String tenForm, String maForm, Pageable pageable);

    List<CauHinhEform> findTop1000By();
}
