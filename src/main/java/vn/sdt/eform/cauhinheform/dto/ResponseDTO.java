package vn.sdt.eform.cauhinheform.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@AllArgsConstructor
public class ResponseDTO<T> {
    private int status;
    private long totalItems;
    private T data;

   public ResponseDTO(int status, T data){
       this.status = status;
       this.data = data;
   }
}
