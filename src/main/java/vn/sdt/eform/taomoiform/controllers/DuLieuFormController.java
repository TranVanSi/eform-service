package vn.sdt.eform.taomoiform.controllers;

import freemarker.template.TemplateException;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import vn.sdt.eform.cauhinheform.dto.ResponseDTO;
import vn.sdt.eform.taomoiform.dto.DuLieuDTO;
import vn.sdt.eform.taomoiform.services.DuLieuFormService;

import java.io.IOException;
import java.net.URLDecoder;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/dulieuform")
public class DuLieuFormController {
    private final DuLieuFormService duLieuFormService;

    @PostMapping
    public void save(@RequestBody DuLieuDTO duLieuDTO,
                     @RequestParam(value = "maForm") String maForm) {
        duLieuFormService.saveDuLieu(duLieuDTO, maForm);
    }

    @GetMapping("/findByMaForm")
    public ResponseDTO findByMaForm (@RequestParam(value = "maForm") String maForm) {
        return duLieuFormService.getListDuLieuByMaForm(maForm);
    }

    @GetMapping("/{objectId}")
    public ResponseDTO findById (@PathVariable String objectId) {
        return duLieuFormService.findById(objectId);
    }

    @DeleteMapping("/delete")
    public boolean deleteGiayTo(@RequestParam(value = "objectId") String objectId) {
        duLieuFormService.deleteDuLieu(objectId);
        return true;
    }

    @GetMapping("/search")
    public ResponseDTO findById (
                                 @RequestParam(value = "maForm") String maForm,
                                 @RequestParam(value = "listTruongSearch") String listTruongSearch,
                                 @RequestParam(value = "listValueSearch") String listValueSearch) throws Exception {
        return duLieuFormService.searchDuLieu(maForm, listTruongSearch, listValueSearch);
    }
    @GetMapping("/filter")
    public ResponseDTO filter(@RequestParam(value = "maForm") String maForm,
                              @RequestParam(value = "listTruongSearch", defaultValue = "") String listTruongSearch,
                              @RequestParam(value = "listValueSearch", defaultValue = "") String listValueSearch,
                              @RequestParam(value = "offset", defaultValue = "0") int offset,
                              @RequestParam(value = "size", defaultValue = "10") int size) throws Exception {
        return duLieuFormService.filter(maForm, listTruongSearch, listValueSearch, offset, size);
    }

    @GetMapping("/getUrlCauHinh")
    public ResponseDTO getUrlCauHinh(@RequestParam(value = "maForm") String maForm,
                              @RequestParam(value = "tenTable", defaultValue = "") String tenTable) {
        return duLieuFormService.getUrlCauHinh(maForm, tenTable);
    }

    @PostMapping("/importFileDuLieu")
    public ResponseDTO importFile(@RequestParam(value = "file") MultipartFile file,
                                  @RequestParam(value = "maForm", defaultValue = "") String maForm,
                                  @RequestParam(value = "listKeyId", defaultValue = "") String listKeyId,
                                  @RequestParam(value = "listKeyMergeId", defaultValue = "") String listKeyMergeId) throws IOException, TemplateException {
        return duLieuFormService.importData(file, maForm, listKeyId, listKeyMergeId);

    }

    @GetMapping("/findone")
    public ResponseDTO findOne(
            @RequestParam(value = "maForm") String maForm,
            @RequestParam(value = "listTruongSearch") String listTruongSearch,
            @RequestParam(value = "listValueSearch") String listValueSearch) {
        return duLieuFormService.findOne(maForm, listTruongSearch, listValueSearch);
    }
}
