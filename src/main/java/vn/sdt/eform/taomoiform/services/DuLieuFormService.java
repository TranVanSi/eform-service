package vn.sdt.eform.taomoiform.services;

import jdk.nashorn.internal.runtime.regexp.RegExp;
import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.json.JSONObject;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.*;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import vn.sdt.eform.cauhinheform.dto.ResponseDTO;
import vn.sdt.eform.cauhinheform.utils.Constants;
import vn.sdt.eform.cauhinheform.utils.EformUtil;
import vn.sdt.eform.model.Status;
import vn.sdt.eform.taomoiform.dto.DuLieuDTO;
import vn.sdt.eform.utils.DateUtils;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DuLieuFormService {
    private final MongoTemplate mongoTemplate;
    private final String dataCollection = Constants.DATA_COLLECTION;

    public void saveDuLieu(DuLieuDTO duLieuDTO, String maForm) {
        if (duLieuDTO.getId() != null){
            Query command = new Query();
            command.addCriteria(Criteria.where("_id").is(new ObjectId(duLieuDTO.getId())));
            mongoTemplate.remove(command, String.class, dataCollection);
        }
        Document doc = Document.parse(duLieuDTO.getDuLieu());
        doc.append("maForm", maForm);
        mongoTemplate.insert(doc, dataCollection);
    }

    public void saveDuLieuAll(List<DuLieuDTO> duLieuDTOs, String maForm) {
        List<Document> documents = new ArrayList<>();
        for (DuLieuDTO item:duLieuDTOs) {
            Document doc = Document.parse(item.getDuLieu());
            doc.append("maForm", maForm);
            documents.add(doc);
        }
        mongoTemplate.insert(documents, dataCollection);
    }

    public void saveDuLieuJson(List<JSONObject> duLieu, String maForm) {
        List<Document> documents = new ArrayList<>();
        for (JSONObject item:duLieu) {
            Document doc = Document.parse(item.toString());
            doc.append("maForm", maForm);
            documents.add(doc);
        }

        mongoTemplate.insert(documents, dataCollection);
    }

    public void updateDuLieuJson(List<JSONObject> duLieu, String maForm) {
        for (JSONObject item:duLieu) {
            Document doc = Document.parse(item.toString());
            doc.append("maForm", maForm);
            mongoTemplate.save(doc, dataCollection);
        }

    }

    public ResponseDTO getListDuLieuByMaForm(String maForm) {
        Query query = new Query();
        query.addCriteria(Criteria.where("maForm").is(maForm));
        return new ResponseDTO(Status.SUCCESS.value(), mongoTemplate.find(query, String.class, dataCollection));
    }

    public ResponseDTO findById(String objectId) {
        Query command = new Query();
        command.addCriteria(Criteria.where("_id").is(new ObjectId(objectId)));
        return new ResponseDTO(Status.SUCCESS.value(), mongoTemplate.findOne(command, String.class, dataCollection));
    }

    public void deleteDuLieu(String objectId) {
        if (objectId != null) {
            Query command = new Query();
            command.addCriteria(Criteria.where("_id").is(new ObjectId(objectId)));
            mongoTemplate.remove(command, String.class, dataCollection);
        }
    }

    public ResponseDTO searchDuLieu(String maForm, String listTruongSearch, String listValueSearch) throws Exception{
        Query command = new Query();
        command.addCriteria(Criteria.where("maForm").is(maForm));
        String[] truongSearch = listTruongSearch.split(",");
        String[] valueSearch = listValueSearch.split(",");

        for (int i = 0; i < truongSearch.length; i++){
            if (truongSearch[i].length() > 0) {
                if (truongSearch[i].toString().equals("maTen")) {
                    command.addCriteria(new Criteria().orOperator(Criteria.where("ma").regex(URLDecoder.decode(valueSearch[i].trim(), "UTF-8"),"i"), Criteria.where("ten").regex(URLDecoder.decode(valueSearch[i].trim(), "UTF-8"),"i")));
                }else{
                    command.addCriteria(Criteria.where(truongSearch[i]).is(URLDecoder.decode(valueSearch[i].trim(), "UTF-8")));
                }
            }
        }
        return new ResponseDTO(Status.SUCCESS.value(),mongoTemplate.find(command, String.class, dataCollection));
    }

    public ResponseDTO filter(String maForm, String listTruongSearch, String listValueSearch, int offset, int size) throws Exception{
        Query query = new Query();
        query.addCriteria(Criteria.where("maForm").is(maForm));
        Query queryCount = new Query();
        query.collation(Collation.of("en").
                strength(Collation.ComparisonLevel.secondary()));
        String[] truongSearch = listTruongSearch.split(",");
        String[] valueSearch = listValueSearch.split(",");
        String tuNgay = "";
        String denNgay = "";
        String truongSearchNgay= "";
        for (int i = 0; i < truongSearch.length; i++){
            if (truongSearch[i].length() > 0) {
                if (truongSearch[i].toString().equals("maTen")) {
                    query.addCriteria(new Criteria().orOperator(Criteria.where("ma").regex(URLDecoder.decode(valueSearch[i].trim(), "UTF-8"), "i"), Criteria.where("ten").regex(URLDecoder.decode(valueSearch[i].trim(), "UTF-8"), "i")));
                }else{
                    if (truongSearch[i].toString().indexOf("tuNgay")>-1){
                        truongSearchNgay = truongSearch[i].toString().substring(6);
                        tuNgay = valueSearch[i].toString();

                    }else {
                        if (truongSearch[i].toString().indexOf("denNgay") > -1) {
                            truongSearchNgay = truongSearch[i].toString().substring(7);
                            denNgay = valueSearch[i].toString();

                        }else{
                            query.addCriteria(Criteria.where(truongSearch[i]).is(URLDecoder.decode(valueSearch[i].trim(), "UTF-8")));
                        }
                    }
                }
            }
        }
        if (tuNgay != "" && denNgay == ""){
            query.addCriteria(Criteria.where(truongSearchNgay).gte(tuNgay));
        }
        if (tuNgay == "" && denNgay != ""){
            query.addCriteria(Criteria.where(truongSearchNgay).lte(denNgay));
        }
        if (tuNgay != "" && denNgay != ""){
            query.addCriteria(Criteria.where(truongSearchNgay).gte(tuNgay).lte(denNgay));
        }

        query.with(new Sort(Sort.Direction.DESC,"ngaySua"));
        queryCount = query;
        query.skip(offset);
        query.limit(size);
        long count = mongoTemplate.count(queryCount, dataCollection);
        return new ResponseDTO(Status.SUCCESS.value(), count, mongoTemplate.find(query, String.class, dataCollection));
    }

    public List<JSONObject> filterIn(String maForm, Map<String, List<String>> listSearch){
        Query query = new Query();
        query.addCriteria(Criteria.where("maForm").is(maForm));
        for (Map.Entry<String, List<String>> item:listSearch.entrySet()){
            String truongSearch = item.getKey();
            query.addCriteria(Criteria.where(truongSearch).in(item.getValue()));
        }
        List<String> list = mongoTemplate.find(query, String.class, dataCollection);
        List<JSONObject> jsonObjects = new ArrayList<>();
        for (String item:list) {
            jsonObjects.add(new JSONObject(item));
        }
        return jsonObjects;
    }

    public ResponseDTO getUrlCauHinh(String maForm, String tenTable) {
        Query command = new Query();
        command.addCriteria(Criteria.where("maCauHinhUrl").is(maForm));
        command.addCriteria(Criteria.where("maForm").is(tenTable));

        return new ResponseDTO(Status.SUCCESS.value(), mongoTemplate.find(command, String.class, Constants.DATA_COLLECTION));
    }

    public ResponseDTO importData(MultipartFile file, String maForm, String keyId, String keyMergeId) throws IOException {
        Workbook workbook = WorkbookFactory.create(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();
        List<JSONObject> duLieu = new ArrayList<>();
        List<String> listLabel = new ArrayList<>();
        Map<String, List<String>> search = new HashMap<>();
        search.put(keyId, new ArrayList<>());
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            Iterator<Cell> cellIterator = row.cellIterator();
            DataFormatter dataFormatter = new DataFormatter();
            if (row.getRowNum() == 0) {
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    String label = dataFormatter.formatCellValue(cell);
                    if (label != null && label.length() > 0) {
                        listLabel.add(cell.getColumnIndex(), label);
                    }
                }
            } else {
                JSONObject jsonObject = new JSONObject();
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    int columnIndex = cell.getColumnIndex();
                    String cellValue = dataFormatter.formatCellValue(cell);
                    if (cellValue != null && cellValue.length() > 0) {
                        String key = listLabel.get(columnIndex);
                        if (keyId.trim().equals(key.trim())) {
                            List<String> listValue = search.get(keyId);
                            listValue.add(cellValue);
                            search.replace(key, listValue);
                        }
                        jsonObject.put(key, cellValue);
                    }
                }
                if (jsonObject.keys().hasNext()) {
                    duLieu.add(jsonObject);
                }
            }
        }
        if (keyId != null && keyId.length() > 0) {
            List<JSONObject> jsonObjects = filterIn(maForm, search);
            Map<Object, Object> mapKeyAndOId = jsonObjects.stream().collect(Collectors.toMap(p -> p.get(keyId), p -> p.get("_id")));
            Map<Object, Object> mapKeyAndMergeValue = null;
            if (keyMergeId != null && keyMergeId.trim().length() > 0) {
                mapKeyAndMergeValue = jsonObjects.stream().collect(Collectors.toMap(p -> p.get(keyId), p -> p.get(keyMergeId)));
            }
            Map<Object, Object> finalMapKeyAndMergeValue = mapKeyAndMergeValue;
            duLieu.stream().forEach(p -> {
                if (p != null && p.has(keyId) && mapKeyAndOId.containsKey(p.get(keyId))) {
                    p.put("_id", mapKeyAndOId.get(p.get(keyId)));
                    if (keyMergeId != null) {
                        String valueDB = finalMapKeyAndMergeValue.get(p.get(keyId)).toString().trim();
                        String valueCur = p.get(keyMergeId).toString().trim();
                        if (EformUtil.isNumeric(valueCur) && EformUtil.isNumeric(valueDB)) {
                            p.put(keyMergeId, (Integer.parseInt(valueDB) + Integer.parseInt(valueCur)) + "");
                        }

                    }
                }
            });
            updateDuLieuJson(duLieu, maForm);
        } else {
            saveDuLieuJson(duLieu, maForm);
        }
        return new ResponseDTO(Status.SUCCESS.value(),null);
    }

    public ResponseDTO findOne(String maForm, String listTruongSearch, String listValueSearch){
        Query command = new Query();
        command.addCriteria(Criteria.where("maForm").is(maForm));
        String[] truongSearch = listTruongSearch.split(",");
        String[] valueSearch = listValueSearch.split(",");
        for (int i = 0; i < truongSearch.length; i++){
            command.addCriteria(Criteria.where(truongSearch[i]).is(valueSearch[i]));
        }
        return new ResponseDTO(Status.SUCCESS.value(),mongoTemplate.findOne(command, String.class, dataCollection));
    }
}
