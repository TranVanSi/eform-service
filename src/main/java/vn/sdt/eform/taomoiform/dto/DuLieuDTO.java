package vn.sdt.eform.taomoiform.dto;

import lombok.Data;

import javax.persistence.Id;

@Data
public class DuLieuDTO {
    @Id
    private String id;
    private String duLieu;
}
